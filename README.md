# snap-conversation

SNAP! extension for AI conversation.

Download the web bridge accordingly to your operating system:

* [WINDOWS](https://bitbucket.org/sprintingkiwi/snap-conversation/raw/master/server-win.exe)
* [MAS OS](https://bitbucket.org/sprintingkiwi/snap-conversation/raw/master/server-macos)
* [LINUX](https://bitbucket.org/sprintingkiwi/snap-conversation/raw/master/server-linux)

... and execute it

Go to https://snap.berkeley.edu/snapsource/snap.html#open:https://bitbucket.org/sprintingkiwi/snap-conversation/raw/master/New_Conversation_Project.xml

Done!
