var express = require("express");
var app = express();

var AssistantV1 = require('watson-developer-cloud/assistant/v1');

// Headers
app.use(function (req, res, next) {
    // res.setHeader("Content-Type", "text/plain");
    res.setHeader("Access-Control-Allow-Origin", "*");
    console.log(req.url);
    next();
});

app.get("/", function (req, res) {
    res.send("SNAP! conversation server running.");
});

function trainSnapIntent(assistant, res, snapWorkspace, intentName, exampleText) {

    console.log("Now checking for an existing intent named: " + intentName);
    try {
        console.log("Listing intents in workspace: " + snapWorkspace);
        var params = {
            workspace_id: snapWorkspace,
        };
        assistant.listIntents(params, function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    console.log(JSON.stringify(response, null, 2));
                    var found = false;
                    response["intents"].forEach(function (intent) {
                        if (intent["intent"] == intentName) {
                            found = true;
                        }
                    });
                    if (found) {
                        console.log("Found intent: " + intentName);
                        console.log("Now adding a new example: " + exampleText);
                        var params = {
                            workspace_id: snapWorkspace,
                            intent: intentName,
                            text: exampleText
                        };

                        assistant.createExample(params, function (err, response) {
                            if (err) {
                                console.log(err);
                                res.send(err);
                                return;
                            } else {
                                console.log(JSON.stringify(response, null, 2));
                                console.log("Successfully added example: " + exampleText);
                                res.send("Successfully added example: " + exampleText);
                            }
                        });
                    } else {
                        try {
                            console.log("Intent not found, creating it...");
                            var params = {
                                workspace_id: snapWorkspace,
                                intent: intentName,
                                examples: [
                                    {
                                        text: exampleText
                                    }
                                ]
                            };
                            assistant.createIntent(params, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    console.log("Successfully created intent with name: " + intentName + " and example: " + exampleText);
                                    res.send("Successfully created intent with name: " + intentName + " and example: " + exampleText);
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
}

app.get("/train-snap-intent/:name/:text/:apikey/:url", function (req, res) {
    var intentName = req.params.name;
    var exampleText = req.params.text;
    var snapWorkspace = "";
    var apikey = req.params.apikey;
    var url = req.params.url;

    console.log("Trying to train intent " + intentName + " with example: " + exampleText);

    var assistant = new AssistantV1({
        version: '2018-09-20',
        iam_apikey: apikey,
        url: url
    });

    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found. Creating a new one.");

                        var params = {
                            name: "TJBot_SNAP_Default",
                            description: 'Workspace created via SNAP!'
                        };
                        assistant.createWorkspace(params, function (err, response) {
                            if (err) {
                                console.log(err);
                                res.send(err);
                                return;
                            } else {
                                try {
                                    snapWorkspace = response["workspace_id"];
                                    console.log("Created new TJBot default workspace with id: " + snapWorkspace);
                                    trainSnapIntent(assistant, res, snapWorkspace, intentName, exampleText);
                                } catch (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                }
                            }
                        });
                    } else {
                        trainSnapIntent(assistant, res, snapWorkspace, intentName, exampleText);
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});

app.get("/find-snap-intent/:intent/:text/:apikey/:url", function (req, res) {
    var intent = req.params.intent;
    var messageText = req.params.text;
    var apikey = req.params.apikey;
    var url = req.params.url;

    console.log("Checking intent in text from SNAP! : " + messageText);

    var assistant = new AssistantV1({
        version: '2018-09-20',
        iam_apikey: apikey,
        url: url
    });
    var snapWorkspace = "";

    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found.");
                        res.send("TJBot Snap default workspace not found.");
                    } else {
                        try {
                            assistant.message({
                                workspace_id: snapWorkspace,
                                alternate_intents: true,
                                input: { 'text': messageText }
                            }, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    try {
                                        var found = false;
                                        response["intents"].forEach(function (element) {
                                            if (element["intent"] == intent && parseFloat(element["confidence"]) > 0.6) {
                                                found = true;
                                            }
                                        });
                                        if (found) {
                                            res.send("true");
                                        } else {
                                            res.send("false");
                                        }
                                        //var snapRes = response["intents"][0]["intent"] + ";" + response["intents"][0]["confidence"];
                                        //res.send(snapRes);
                                    } catch (err) {
                                        console.log(err);
                                        res.send(err);
                                        return;
                                    }
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});

app.get("/status-snap-workspace/:apikey/:url", function (req, res) {
    console.log("Getting status info about SNAP! conversation workspace");

    var apikey = req.params.apikey;
    var url = req.params.url;

    var assistant = new AssistantV1({
        version: '2018-09-20',
        iam_apikey: apikey,
        url: url
    });
    var snapWorkspace = "";

    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found.");
                        res.send("TJBot Snap default workspace not found.");
                    } else {
                        try {
                            var params = {
                                workspace_id: snapWorkspace
                            };
                            assistant.getWorkspace(params, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    try {
                                        var snapRes = response["status"];
                                        res.send(snapRes);
                                    } catch (err) {
                                        console.log(err);
                                        res.send(err);
                                        return;
                                    }
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});

app.get("/delete-snap-workspace/:apikey/:url", function (req, res) {
    console.log("Deleting SNAP! workspace to reset conversation");

    var apikey = req.params.apikey;
    var url = req.params.url;

    var assistant = new AssistantV1({
        version: '2018-09-20',
        iam_apikey: apikey,
        url: url
    });
    var snapWorkspace = "";

    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found.");
                        res.send("TJBot Snap default workspace not found.");
                    } else {
                        try {
                            var params = {
                                workspace_id: snapWorkspace
                            };
                            assistant.deleteWorkspace(params, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    console.log("Successfully deleted workspace: " + snapWorkspace);
                                    res.send("Successfully deleted workspace: " + snapWorkspace);
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});


app.listen(3000, function () {
    console.log("SNAP conversation server running on port 3000!");
});